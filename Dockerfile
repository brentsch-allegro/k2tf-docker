FROM debian:buster-slim

RUN apt-get update -y && apt-get install -y curl

RUN curl -Lo /tmp/k2tf.tar.gz https://github.com/sl1pm4t/k2tf/releases/download/v0.5.0/k2tf_0.5.0_Linux_x86_64.tar.gz && \
  tar -xf /tmp/k2tf.tar.gz -C /usr/local/bin && \
  chmod 555 /usr/local/bin/k2tf

USER 1001

ENTRYPOINT ["/usr/local/bin/k2tf"]
